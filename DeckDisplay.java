/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CardShuffle;

/**
 *
 * @author 2ndyrGroupA
 */
public class DeckDisplay {
    public static void main(String[] args){
        Deck deck = new Deck();
        deck.shuffling();
        for (int suit=1; suit<=Deck.numSuits; suit++){
            for (int rank=1; rank<=Deck.numRanks; rank++){
                Card card = deck.getCard(suit, rank);
                //System.out.println(card.rank + " "+ card.suit);
                System.out.println(card.rankToName(card.rank)+" "+card.suitToName(card.suit));
            }
        }
        
    }
}

